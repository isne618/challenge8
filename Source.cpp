#include<iostream>
#include<list>
using namespace std;

int main()
{
	list<int>people; //collection of node
	list<int>::iterator pt; //pointer
	list<int>::iterator pt_next; //point to next one
	list<int>::iterator last;//the last node
	int p, move, i;
	cout << "     --HOT POTATO--" << endl;
	cout << "-------------------------" << endl;
	cout << "Input number of people: ";
	cin >> p;
	cout << "Input number of move: ";
	cin >> move;
	cout << "-------------------------" << endl;
	for (i = 1; i <= p; i++)
	{
		people.push_back(i); //add number of people
	}
	pt = people.begin(); //pointer is point to the first node
	cout << "Now potato is at " << *pt << endl;
	cout << "-------------------------" << endl;
	while (people.size() > 1)
	{
		for (pt_next = people.begin(); pt_next != people.end(); pt_next++)
		{
			cout << *pt_next << " ";
		}
		cout << endl;
		last = people.end();
		last--;
		for (i = 0; i < move; i++)
		{
			if (pt == last)
			{
				pt = people.begin();
			}
			else
			{
				pt++;
			}
		}
		cout << "#" << *pt << " --> DIED!!" << endl;
		cout << "-------------------------" << endl;
		pt_next = pt;
		if (pt == last) //if it point to the last node
		{
			pt = people.begin(); //start again at the first node
		}
		else
		{
			pt++;
		}
		cout << "Now potato is at " << *pt << endl;
		people.erase(pt_next);
	}
	cout << "-------------------------" << endl;
	cout << "#" << *(people.begin()) << " --> WIN!!!" << endl;
	cout << endl;
	
	system("pause");
	return 0;
}